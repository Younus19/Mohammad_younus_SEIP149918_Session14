<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../Assest/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../../Assest/bootstrap-3.3.7-dist/js/jquery.min.js"></script>
    <script src="../../Assest/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../../Assest/bootstrap-3.3.7-dist/css/bootstrap.css"></script>
</head>
<body>

<div class="container">
    <h2 style="color: green">Vertical (basic) form</h2>
    <form>
        <div class="form-group">
            <label for="email">Email:</label>
            <input style="border-color: fuchsia;background-color:navajowhite" type="email" class="form-control " id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input  style="border-color: fuchsia " type="password" class="form-control" id="pwd" placeholder="Enter password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn-lg btn-success">Submit</button>
    </form>
</div>

</body>
</html>

